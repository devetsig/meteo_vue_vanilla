var app = new Vue({
    el: "#app",
    data: {
      chart: null,//l'objet Chart !
      city: "",//nom de la ville qu'on entrera dans le INPUT
      dates: [],//les dates de l'API
      temps: [],//les températures de l'API
      loading: false,
      errored: false,
      map : null,
      tileLayers : null,
      marker: null
    },
    mounted() {//ce seront les fonctions qui se déclencheront en premier !
      this.createMap();//on souhaite que la map soit créée avant qu'on entre le nom de la ville
  },
    methods: {//quand on met plusieurs fonctions dans methods, ATTENTION aux hooks !
      getData: function() {
        this.loading = true;
  
        if (this.chart != null) {//cette condition sera vérifiée à chaque fois qu'on lancer getData()
          this.chart.destroy();//donc à chaque fois qu'on sélectionnera une nouvelle ville
        }

        axios //important de préciser qu'on va écrire l'essentiel du code DANS la promesse !
          .get("https://api.openweathermap.org/data/2.5/forecast", {
            params: {//on préciser les variables en paramètres
              q: this.city,
              units: "metric",//Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit
              appid: "3ad6af6152484436e8190e7721c31dc1"
            } //c'est l'équivalent de http://api.openweathermap.org/data/2.5/forecast?q=oran&APPID=3ad6af6152484436e8190e7721c31dc1&units=celsius
          })
          .then(response => {//un seul THEN mais on stocke 2 data (dates et températures)
            this.dates = response.data.list.map(list => {//les dates sous la forme de tableau
              return list.dt_txt;
            });
  
            this.temps = response.data.list.map(list => {//les températures idem car il y en a bcp
              return list.main.temp;
            });

            //pour affiche les coordonnées du point de relevés des Températures sur la map:
            //il faut que le marker soit NULL ou sinon qu'on le retire pour pouvoir REITERER autant de fois 
            //qu'on le souhaite cette boucle
            if (this.marker != null) {
              this.marker.remove();//la méthode remove() permet de retirer un marker leaflet
            }

            this.marker = L.marker(response.data.city.coord).bindPopup(
              '<div>Ville : ' + response.data.city.name + 
              '</div><div>Population : ' + response.data.city.population +
              ' habitants</div>'
              ).addTo(this.map).openPopup(); //la méthode .openPopup() vient toujours après le addTo()
              
            this.map = this.map.setView(response.data.city.coord, 8);

            //pr créer un chart, il faut utiliser l'objet Chart de ChartJS qui prend en paramètres
            //deux objets : ctx ET un objet {type, data, options}
            var ctx = document.getElementById("myChart");// correspond au canevas html allant accueillir le chart

            this.chart = new Chart(ctx, {
              type: "line", //type du chart (line, bar, etc)
              data: {//contient les datasets et les labels
                labels: this.dates,//là c'est les dates de l'API en abscisses
                datasets: [//plein de paramètre MAIS SURTOUT les données température de l'API à rentrer
                  {
                    label: "Températures moyennes",
                    backgroundColor: "rgba(54, 162, 235, 0.5)",//c'est le backgroundcolor de la légende
                    borderColor: "rgb(54, 162, 235)",
                    pointBackgroundColor: 'white',//pr les points
                    fill: false,
                    data: this.temps //data API à entrer ici pour les faire apparaître en ordonnées
                  }
                ]
              },
              options: {//pr gérer le titre, les tooltips(infobulles), les échelles
                responsive: true,//on veut que l'appli soit responsive
                maintainAspectRatio:false,//obligé de mettre ça pr que le responsive du chart fctionne
                title: {
                  display: true,
                  text: "Prévisions météorologiques (Températures en C°) avec l'API OpenWeatherMap"
                },
                tooltips: {// ttes les explains sur les tooltips ici : https://www.chartjs.org/docs/latest/configuration/tooltip.html
                  callbacks: {
                    label: function(tooltipItem, data) {
                      var label =
                        data.datasets[tooltipItem.datasetIndex].label || "";
  
                      if (label) {
                        label += ": ";
                      }
  
                      label += Math.floor(tooltipItem.yLabel);
                      return label + "°C";
                    }
                  }
                },
                scales: {
                  xAxes: [
                    {
                      type: "time",
                      time: {
                        unit: "hour",
                        displayFormats: {
                          hour: "DD-MM, HH:mm"
                        },
                        tooltipFormat: "DD MMMM, HH:mm"
                      },
                      scaleLabel: {
                        display: true,
                        labelString: "Date"
                      }
                    }
                  ],
                  yAxes: [
                    {
                      scaleLabel: {
                        display: true,
                        labelString: "Température (°C)"
                      },
                      ticks: {
                        beginAtZero: false,
                        callback: function(value, index, values) {
                          return value + "°C";
                        }
                      }
                    }
                  ]
                }
              }
            });
          })
          .catch(error => {
            console.log(error);
            this.errored = true;
          })
          .finally(() => (this.loading = false));
      },
      createMap: function () {
        this.map = L.map('mymap').setView([49.03898, 2.07501], 4);
        this.tileLayers = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
        {
          maxZoom: 18,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>',
        });
        this.tileLayers.addTo(this.map);
      }
    }
  });
  
